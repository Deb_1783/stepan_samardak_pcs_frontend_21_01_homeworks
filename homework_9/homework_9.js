'use strict';

// Получаем данные пользователей методом fetch

fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => response.json())
    .then((resData) => {

        /* Задаю время для задержки вывода данных в 1 сек
        Вывожу полученные данные в массив
        */

        setTimeout(() => {
            console.log('-----------');
            console.log('Пункт №1: Получаем данные всех пользователей...')
            console.log('-----------');
            console.log(resData);
        }, 1000);

        /*
        Задаю время для задержки вывода данных в 2,5 сек
        Вывожу фамилии всех пользователей через .forEach
        */

        const dataOfUsers = resData.data;
        setTimeout(() => {
            console.log('-----------');
            console.log('Пункт №2: Выводим фамилии всех пользователей...');
            console.log('-----------');
            dataOfUsers.forEach((item) => {
                console.log(item?.last_name);
            })
        }, 2500);

        /*
        Задаю время для задержки вывода данных в 4 сек
        Методом .filter задаю условие вывести все фамилии, начинающиеся на F
        */

        setTimeout(() => {
            console.log('-----------');
            console.log('Пункт №3: Выводим данные всех пользователей, фамилия которых начинается на F...');
            console.log('-----------');
            const lastNameOfF = resData.data;
            const filterArr = lastNameOfF.filter(item => item.last_name[0] === 'F');
            console.log(filterArr);
        }, 4000);

        /*
        Задаю время для задержки вывода данных в 5 сек
        Методом .reduce аккумулирую значения имени и фамилии
        */

        setTimeout(() => {
            console.log('-----------');
            console.log('Пункт №4: Выводим имена и фамилии всех пользователей...');
            console.log('-----------');
            const arrString = resData.data.reduce((acc, item) => {
                return acc + ` ${item.first_name} ${item.last_name}, `
                /* or
                const result = `${acc} ${item.first_name} ${item.last_name}, `;
                return result;
                 */
            }, 'Наша база содержит данные следующих пользователей: ');
            console.log(arrString);
        }, 5000)

        /*
        Задаю время для задержки вывода данных в 6 сек
        Методом .reduce также аккумулирую значения 
        и через Object.asign вывожу все ключи пользователей 
        */

        setTimeout(() => {
            console.log('-----------');
            console.log('Пункт №5: Выводим названия всех ключей в объекте пользователя...');
            console.log('-----------');
            const allKeys = resData.data.reduce((acc, item) => {
                const sum = Object.assign(acc, {[item.first_name]: `id: ${item.id}, last_name: ${item.last_name}, email: ${item.email},`});
                return sum;
            }, {});
            console.log(allKeys);
        }, 6000)

        // В случае ошибки будет выводится сообщение с ошибкой получения данных

        .catch(err => console.error(err));
    });
