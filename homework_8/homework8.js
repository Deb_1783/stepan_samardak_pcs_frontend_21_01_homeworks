'use strict'

function makeFibonacciFunction() {
    let n = 0;
    let firstNumber = 0, secondNumber = 1;
    function add() {
        if (n === 0) {
            n++;
            console.log(n);
            return firstNumber;
        }
        let result = firstNumber + secondNumber;
        firstNumber = secondNumber;
        secondNumber = result;
        console.log(result);
        return result;
    }
    return add;
}

const fibonacci = makeFibonacciFunction();
fibonacci()
fibonacci()
fibonacci()
fibonacci()
fibonacci()
fibonacci()
fibonacci()
fibonacci()







